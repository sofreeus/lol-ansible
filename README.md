# Ansible... LOL!

The scope for this demo is to feature a few of the main
advantages of ansible.  Maybe mention some of the other config
mgmt systems we have used.

# Contrast ansible with:

- shell scripts
- Cleverness involving perl/python or some other scripting
  languge.
- Ancient tools like rdist, cfengine
- More modern ones like puppet, salt stack, chef, etc

- Playbooks may be more readable to more people than
  shell-script or perl.

- Ansible Inventory (/etc/ansible/hosts) is more readable than puppet
  site manifest (/etc/puppet/manifests/site.pp)
- Ansible inventory is not necessarily hierarchical.  It
  supports arbitrary grouping and duplication of hosts.
- Ansible requres no non-standard agents on clients.
  Requirements: local/ssh, python 2, python-simplejson
  http://docs.ansible.com/ansible/intro_installation.html#managed-node-requirements
- It does need a user that you can login as via ssh.
- It also needs root or become (sudo) to do any real work.

# Components:

- To run the demo you'll need linux desktop or vm in which to run the
  demo.
- As a practical matter we'll be making a simple webserver
  sharing a CRUD app using rapidapp http://www.rapidapp.info/

# Info

- Ansible docs: http://docs.ansible.com/
- sfsuser: sees9OhS

# Demo

- Use the ansible 'package' module to isolate from distro
  variants
- pull demo from the gitlab repo.
  https://gitlab.com/sofreeus/lol-ansible/tree/coffee-shop
- Demo uses localhost only.  

```
                             .-----------------------.
                     |       |     Linux Server      |
                     |       |                       |
    __  _            |       |      .--------.       |
   [__]|=|-----------|------------->| nginex |       |
   /::/|_|           |       |      '--------'       |
                     |       |           |           |
                     |       |           v           |
    __  _            |       |     .----------.      |
   [__]|=|-----------|       |     | rapidapp |      |
   /::/|_|           |       |     '----------'      |
                     |       |           |           |
                     |       |           v           |
                     |       |      .--------.       |
    __  _            |       |      | sqlite |       |
   [__]|=|-----------|       |      '--------'       |
   /::/|_|           |       |                       |
                     |       '-----------------------'
                     |
```

## Steps

- describe the goal
- Preparation
- Look at the files
  - inventory
  - playbook
- Run a couple adhoc commands
- Run a playbook
- Demonstrate that it worked
  http://localhost

## Details

- Check out the demo repo
- Start two terminals running bash. 
- Change to the lol-ansible/demo directory in both `cd
  lol-ansible/demo`
- In the first terminal we will run the ansible environment:
- In the second we'll test out the clients functionality: 

## Demo Commands

Here we execute the actual demo.

### In the first window
- Change to the demo directory `cd demo`
- Start the container: `start_demo`
- Paste in the `sfsuser` password from above at the promopt
- Run the these commands

```
cd /demo
source environment
ssh-keyscan localhost >> ~/.ssh/known_hosts
ansible all -m ping -k
sshpass -e ansible-playbook /demo/playbook/install_ssh_key.yml
ansible-playbook /demo/playbook/install_ssh_key.yml
ansible-playbook /demo/playbook/setup_webserver.yml

```

### In the second window

Now go back and check that the webserver is doing someting.
- Get the IP for the running container: `./getip`
- Start your browser and go to the IP address:

# Lab

Attendees run the demo.

# Question Time
